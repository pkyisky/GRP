import sys,os
os.environ['ETS_TOOLKIT'] = 'qt4'

from pyface.qt import QtGui, QtCore

# from PyQt4 import QtCore
# from PyQt4 import QtGui

from traits.api import HasTraits, Instance, on_trait_change
from traitsui.api import View, Item
from mayavi.core.ui.api import MayaviScene, MlabSceneModel, \
        SceneEditor
import numpy as np
from numpy import *
from scipy.special import sph_harm
from mayavi.mlab import *

import matplotlib.pyplot as plt
import matplotlib.cm as cm

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

import time
from datetime import datetime

import subprocess
import psutil
import urllib2
from wifi import Cell, Scheme

import gettext

global lang
lang = "English"

global q

q = 100

interface = "wlan0"


ROUNDED_STYLE_SHEET1 = """QPushButton {
    background-color: #4CAF50;
    border: none;
    color: white;
    width: 57px;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    font-size: 16px;
    margin: 4px 2px;

 }
"""
ROUNDED_STYLE_SHEET2 = """QPushButton {
     background-color: #f44336;
     border: none;
     color: white;
     width: 100px;
     padding: 15px 32px;
     text-align: center;
     text-decoration: none;
     font-size: 16px;
     margin: 4px 2px;
 }
"""
ROUNDED_STYLE_SHEET3 = """QPushButton {
     background-color: #008CBA;
     border: none;
     color: white;
     width: 57px;
     padding: 15px 32px;
     text-align: center;
     text-decoration: none;
     font-size: 16px;
     margin: 4px 2px;
 }
"""

ROUNDED_STYLE_SHEET4 = """QPushButton {
     background-color:  #111111;
     border: none;
     color: white;
     padding: 15px 32px;
     text-align: center;
     text-decoration: none;
     font-size: 16px;
     margin: 4px 2px;
 }
"""



class MplCanvas(FigureCanvas):
    def __init__(self, parent = None, width = 1, height = 1):
        self.figure = Figure(figsize = (width, height))
        self.axes = self.figure.add_subplot(111)
        # We want the axes cleared every time plot() is called
        self.axes.hold(False)

        FigureCanvas.__init__(self, self.figure)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        self.update_figure()


    def update_figure(self):


        X = np.linspace(0,1,100)
        Y = X.copy()
        X, Y = np.meshgrid(X, Y)
        alpha = np.radians(25)
        cX, cY = 0.5, 0.5
        sigX, sigY = 0.2, 0.3
        rX = np.cos(alpha) * (X-cX) - np.sin(alpha) * (Y-cY) + cX
        rY = np.sin(alpha) * (X-cX) + np.cos(alpha) * (Y-cY) + cY

        Z = (rX-cX)*np.exp(-((rX-cX)/sigX)**2) * np.exp(- ((rY-cY)/sigY)**2)

        #print q

        slic = np.s_[0:q,0:q]

        x = X[slic]
        y = Y[slic]
        z = Z[slic]

        #cpf = self.axes.contourf(x,y,z, 25, cmap=cm.seismic)
        #colours = ['w' if level<0 else 'k' for level in cpf.levels]

        cp = self.axes.contour(x, y, z, 25, colors=('r','b','g','m','c','y','k'))

        # data = np.clip(np.random.randn(250, 250), -2, 2)
        #
        # cax = self.axes.imshow(data, interpolation='nearest', cmap=cm.coolwarm)
        cbar = self.figure.colorbar(cp, ticks=[-2, -1, 0, 1, 2])
        #cbar.self.axes.set_yticklabels(['-2', '< -1', '0', '> 1', '2'])
        #self.axes.contour(X, Y, Z)
        self.axes.grid(True)

        self.axes.set(xlabel='X-axis', ylabel='Y-axis', title='Sectional Graph')

        self.draw()


class Visualization(HasTraits):
    scene = Instance(MlabSceneModel, ())

    @on_trait_change('scene.activated')
    def update_plot(self):
        def f(x, y):
            sin, cos = np.sin, np.cos
            return sin(x + y) + sin(1.1 * x - y) + cos(1.2 * x + 2 * y)

        x, y = np.mgrid[-4.:4.05:0.1, -2.:2.05:0.05]

        s = surf(x, y, f, colormap="Oranges")
        s = outline()
        s = axes()
        s = orientation_axes()
        #cs = contour_surf(x, y, f, contour_z=0)

        #self.scene.mlab.mesh(r[0],r[1],r[2], scalars=Y_lm.real, colormap="cool")

    view = View(Item('scene', editor=SceneEditor(scene_class=MayaviScene),
                     height=250, width=300, show_label=False),
                resizable=True # We need this to resize with the parent widget
                )



class Setting():

    WIDTH = 100
    HEIGHT = 100
    #NUM_BLOCKS_X = 2
    #NUM_BLOCKS_Y = 2




class QS(QtGui.QGraphicsScene):

    def __init__(self, *args, **kwargs):
        super(QS, self).__init__(*args, **kwargs)

        print "Inserted in QS col = {}, row = {}".format(X,Y)

        width = X * Setting.WIDTH
        height = Y * Setting.HEIGHT
        self.setSceneRect(0, 0, width, height)
        self.setItemIndexMethod(QtGui.QGraphicsScene.NoIndex)

        for x in range(0,X+1):
            xc = x * Setting.WIDTH
            self.addLine(xc,0,xc,height)

        for y in range(0,Y+1):
            yc = y * Setting.HEIGHT
            self.addLine(0,yc,width,yc)


class QV(QtGui.QGraphicsView, QtGui.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(QV, self).__init__(*args, **kwargs)


class Settings(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(Settings, self).__init__(parent)

        self.powerBtn = QtGui.QToolButton()
        self.powerBtn.setIcon(QtGui.QIcon("./img/power1.png"))
        self.powerBtn.setIconSize(QtCore.QSize(50,50))
        self.powerBtn.setCheckable(True)
        self.powerBtn.setStyleSheet("QToolButton { border: 0; background: transparent; width: 30px; height: 30px; }")
        self.powerBtn.setToolTip( _("Quit") )
        self.powerBtn.toggled.connect(self.close)

        spacer = QtGui.QWidget(self)
        spacer.setSizePolicy(1|2|4,1|4)

        self.toolbar = QtGui.QToolBar(self)
        self.addToolBar(QtCore.Qt.TopToolBarArea, self.toolbar)

        self.toolbar.addWidget(spacer)

        self.toolbar.addSeparator()
        self.toolbar.addWidget(self.powerBtn)

        self.adminBtn = QtGui.QToolButton()
        self.adminBtn.setText( _("Settings"))
        self.adminBtn.setIcon(QtGui.QIcon("./img/setting.png"))
        self.adminBtn.setIconSize(QtCore.QSize(300,300))

        self.adminBtn.setStyleSheet("QToolButton { border: 0; background: transparent; width: 80px; height: 150px; }")

        #self.adminBtn.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)

        self.scrollArea = QtGui.QScrollArea()
        self.scrollArea.setBackgroundRole(QtGui.QPalette.Light)
        #self.scrollArea.setWidget(self.textEdit)
        self.scrollArea.setWidgetResizable(True)

        self.accBtn = QtGui.QPushButton( _("Account") )
        self.accBtn.setStyleSheet(ROUNDED_STYLE_SHEET3)
        self.accBtn.clicked.connect(self.showLogin)

        self.icon1 = QtGui.QIcon()
        self.icon1.addPixmap(QtGui.QPixmap("./img/account.png"),QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.accBtn.setIcon(self.icon1)
        self.accBtn.setIconSize(QtCore.QSize(35,40))

        self.deviceBtn = QtGui.QPushButton( _("Device") )
        self.deviceBtn.setStyleSheet(ROUNDED_STYLE_SHEET2)
        self.deviceBtn.clicked.connect(self.showNetworks)

        self.icon2 = QtGui.QIcon()
        self.icon2.addPixmap(QtGui.QPixmap("./img/device.png"),QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.deviceBtn.setIcon(self.icon2)
        self.deviceBtn.setIconSize(QtCore.QSize(35,40))


        self.viewBtn = QtGui.QPushButton( _("View") )
        self.viewBtn.setStyleSheet(ROUNDED_STYLE_SHEET1)

        self.icon3 = QtGui.QIcon()
        self.icon3.addPixmap(QtGui.QPixmap("./img/setting.png"),QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.viewBtn.setIcon(self.icon3)
        self.viewBtn.setIconSize(QtCore.QSize(35,40))
        self.viewBtn.clicked.connect(self.showSettings)

        self.lbl1 = QtGui.QLabel( _("Settings") )
        self.lbl1.setStyleSheet("QLabel { border: 0; background: transparent; font-size: 20px; text-align: center; }")

        self.vlbl = QtGui.QLabel("version 1.0")

        self.vbox = QtGui.QVBoxLayout()

        self.vbox.addWidget(self.lbl1)
        self.vbox.addWidget(self.adminBtn)
        self.vbox.addWidget(self.accBtn)
        self.vbox.addWidget(self.deviceBtn)
        self.vbox.addWidget(self.viewBtn)
        self.vbox.addWidget(self.vlbl,QtCore.Qt.AlignBottom)

        self.mainLayout = QtGui.QGridLayout()

        self.mainLayout.addLayout(self.vbox,0,0)
        self.mainLayout.addWidget(self.scrollArea,0,1)

        self.setCentralWidget(QtGui.QWidget(self))
        self.centralWidget().setLayout(self.mainLayout)

        self.setGeometry(100,100,800,500)
        self.show()

    def showLogin(self):
        self.newwidget = QtGui.QWidget()
        self.grid = QtGui.QGridLayout(self.newwidget)

        self.iconBtn = QtGui.QToolButton()
        self.iconBtn.setIcon(QtGui.QIcon("./img/admin.png"))
        self.iconBtn.setIconSize(QtCore.QSize(150,150))
        self.iconBtn.setStyleSheet("QToolButton { border: 0; background: transparent; width: 80px; height: 150px; }")


        self.elbl = QtGui.QLabel( _("Email:") )
        self.passlbl = QtGui.QLabel( _("Password:") )
        self.eline = QtGui.QLineEdit()
        self.eline.setPlaceholderText( _("Enter Username...") )
        self.eline.setStyleSheet("QLineEdit { width: 100%; padding: 12px 20px; margin: 8px 0; border: 1px solid #000; }")

        self.passline = QtGui.QLineEdit()
        self.passline.setPlaceholderText( _("Enter Password...") )
        self.passline.setEchoMode(QtGui.QLineEdit.Password)
        self.passline.setStyleSheet("QLineEdit { width: 100%; padding: 12px 20px; margin: 8px 0; border: 1px solid #000; }")


        self.loginBtn = QtGui.QPushButton( _("Login"))
        self.loginBtn.clicked.connect(self.checkLogin)
        self.loginBtn.setStyleSheet("QPushButton{ background-color: #2a1b82; color: white;  padding: 14px 20px;margin: 8px 0; border: none;  width: 100%;}")

        self.forgotBtn = QtGui.QPushButton( _("Forgot Password?") )
        self.forgotBtn.clicked.connect(self.forgotPass)
        self.forgotBtn.setStyleSheet("QPushButton{ background-color: #af520a; color: white;  padding: 14px 20px;margin: 8px 0; border: none;  width: 140%;}")


        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addWidget(self.loginBtn)
        self.vbox.addWidget(self.forgotBtn)

        self.grid.addWidget(self.iconBtn,0,1,QtCore.Qt.AlignCenter)
        self.grid.addWidget(self.elbl,1,0)
        self.grid.addWidget(self.eline,1,1)
        self.grid.addWidget(self.passlbl,2,0)
        self.grid.addWidget(self.passline,2,1)
        self.grid.addLayout(self.vbox,3,1, QtCore.Qt.AlignCenter)
        #self.grid.addWidget(self.loginBtn,2,0)
        #self.grid.addWidget(self.forgotBtn,3,0)
        self.grid.setAlignment(QtCore.Qt.AlignCenter)
        #self.grid.setRowMinimumHeight(2,120)
        #self.grid.addLayout(self.form,0,0,QtCore.Qt.AlignCenter)
        self.scrollArea.setWidget(self.newwidget)

    def forgotPass(self):
        self.newwidget = QtGui.QWidget()
        self.grid = QtGui.QGridLayout(self.newwidget)

        self.elbl = QtGui.QLabel( _("Email:") )
        self.eline = QtGui.QLineEdit()
        self.eline.setPlaceholderText( _("Enter Your Email...") )
        self.eline.setStyleSheet("QLineEdit { width: 100%; padding: 12px 20px; margin: 8px 0; border: 1px solid #000; }")

        self.sendBtn = QtGui.QPushButton( _("Send") )
        self.sendBtn.clicked.connect(self.checkFun)
        self.sendBtn.setStyleSheet("QPushButton{ background-color: #0484f4; color: white;  padding: 14px 20px;margin: 8px 0; border: none;  width: 105%;}")

        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addWidget(self.sendBtn)

        self.grid.addWidget(self.elbl,0,0)
        self.grid.addWidget(self.eline,0,1)
        self.grid.addLayout(self.vbox,1,1,QtCore.Qt.AlignCenter)

        self.grid.setAlignment(QtCore.Qt.AlignCenter)
        self.grid.setRowMinimumHeight(1,90)
        self.scrollArea.setWidget(self.newwidget)

    def checkFun(self):
        text = self.eline.text()
        if text == "":
            self.msg = QtGui.QMessageBox()
            self.msg.setIconPixmap(QtGui.QPixmap("./img/warning.png"))
            self.msg.setText( _("Please Enter Valid Email") )
            self.msg.setWindowTitle( _("Email Not Valid") )
            self.msg.setStandardButtons(QtGui.QMessageBox.Retry)
            self.msg.exec_()
            self.forgotPass()
        else:
            self.showLogin()

    def checkLogin(self):
            text = self.eline.text()
            if text == "":
                self.msg = QtGui.QMessageBox()
                self.msg.setIconPixmap(QtGui.QPixmap("./img/warning.png"))
                #self.msg.setIcon(QtGui.QMessageBox.Warning)
                self.msg.setText( _("Please Enter Valid Email") )
                self.msg.setWindowTitle( _("Email Not Valid") )
                self.msg.setStandardButtons(QtGui.QMessageBox.Ok)
                #self.msg.setStyleSheet("QMessageBox{background-color:#dddddd; color:white;}")
                self.msg.exec_()

            elif self.passline.text() == "":
                self.msg = QtGui.QMessageBox()
                self.msg.setIconPixmap(QtGui.QPixmap("./img/warning.png"))
                self.msg.setText( _("Please Enter Password") )
                self.msg.setWindowTitle( _("Password Not Valid") )
                self.msg.setStandardButtons(QtGui.QMessageBox.Retry)
                self.msg.exec_()
            else:
                self.newwidget = QtGui.QWidget()
                self.grid = QtGui.QGridLayout(self.newwidget)

                self.adlbl = QtGui.QLabel( _("Admin") )
                #self.eline.setText("")
                self.logoutBtn = QtGui.QPushButton( _("Logout") )
                self.logoutBtn.setStyleSheet("QPushButton{ background-color: #c43700; color: white;  padding: 14px 20px;margin: 8px 0; border: none;  width: 100%;}")
                self.logoutBtn.clicked.connect(self.showLogin)
                self.vbox = QtGui.QVBoxLayout()
                self.vbox.addWidget(self.logoutBtn)

                self.grid.addWidget(self.iconBtn,0,1,QtCore.Qt.AlignCenter)
                self.grid.addWidget(self.adlbl,1,1,QtCore.Qt.AlignCenter)
                #self.grid.addWidget(self.eline,1,1)
                self.grid.addLayout(self.vbox,2,1,QtCore.Qt.AlignCenter)
                self.grid.setAlignment(QtCore.Qt.AlignCenter)
                self.scrollArea.setWidget(self.newwidget)

    def showNetworks(self):

        if connected:
            self.newwidget = QtGui.QWidget()
            self.vlayout = QtGui.QGridLayout(self.newwidget)

            l = []
            l = Cell.all('wlan0')
            x = str(l[0])
            y = x[10:-1]

            self.s = QtGui.QLabel()
            self.s.setText(y)
            self.s.setStyleSheet("QLabel {color:black; font-size: 20px;}")

            self.btn = QtGui.QPushButton( _("Disconnect") )
            self.btn.setStyleSheet("QPushButton { background-color: #ff0011; border: none; color: white; width: 90px; padding: 8px 8px; text-align: center; text-decoration: none;font-size: 16px; margin: 4px 2px; }")
            self.btn.clicked.connect(self.disConnet)

            self.vlayout.addWidget(self.s,0,0)
            self.vlayout.addWidget(self.btn,0,1)

            self.vlayout.setAlignment(QtCore.Qt.AlignCenter)
            self.scrollArea.setWidget(self.newwidget)

        else:
            self.newwidget = QtGui.QWidget()
            self.vlayout = QtGui.QGridLayout(self.newwidget)
            l = []
            l = Cell.all('wlan0')
            for i,value in enumerate(l):
                self.s = "lbl" + str(i)
                #print self.s
                self.s = QtGui.QLabel()
                self.s.installEventFilter(self)


                x = str(l[i])

                y = x[10:-1]
                #print y
                self.s.setText(y)
                self.s.setStyleSheet("QLabel {color:black; font-size: 20px;}")
                self.vlayout.addWidget(self.s,i,0)
                self.vlayout.setRowMinimumHeight(i,40)

            self.refreshBtn = QtGui.QPushButton("Refresh")
            self.refreshBtn.setStyleSheet(ROUNDED_STYLE_SHEET3)
            self.refreshBtn.clicked.connect(self.showNetworks)
            self.vlayout.addWidget(self.refreshBtn)
            self.vlayout.setAlignment(QtCore.Qt.AlignCenter)
            self.scrollArea.setWidget(self.newwidget)


    def eventFilter(self, source, event):
        if event.type() == QtCore.QEvent.MouseButtonPress:
            # print "The sender is:", source.text()
            self.network = str(source.text())
            self.showDialog()

        return super(Settings, self).eventFilter(source, event)

    def showDialog(self):

        self.text,ok = QtGui.QInputDialog.getText(self, 'Wifi Authentication','Enter Password:')

        self.tpass = str(self.text)

        if ok:
            self.connection()

    def connection(self):

        status = os.popen("ifconfig wlan0 up").read()

        winame = "wlan0"

        stream = os.popen("iwlist " + winame + " scan")

        if self.tpass == '':
            os.popen("iwconfig " + winame + " essid "+ self.network)
        else:
            connectstatus = os.popen("iwconfig " + winame + " essid " + self.network + " key s:" + self.tpass)

        os.popen("dhclient " + winame)
        ontest = os.popen("ping -c 1 google.com").read()

        if ontest == '':
            print "Connection failed. (Bad pass?)"

        print "Connected successfully!"

    def disConnet(self):
        os.popen("ifconfig wlan0 down")

    def showSettings(self):
        self.newwidget = QtGui.QWidget()
        self.vlayout = QtGui.QGridLayout(self.newwidget)
        self.check1 = QtGui.QCheckBox( _("Close Window") )
        self.check1.stateChanged.connect(self.close)
        self.check2 = QtGui.QCheckBox( _("Settings") )
        self.check3 = QtGui.QCheckBox( _("zoom In") )
        self.check4 = QtGui.QCheckBox( _("zoom Out") )
        #self.check4.setStyleSheet("QCheckBox::indicator:checked {background-color:red; color :#2196F3;}")

        self.vlayout.addWidget(self.check1,0,0)
        self.vlayout.addWidget(self.check2,1,0)
        self.vlayout.addWidget(self.check3,2,0)
        self.vlayout.addWidget(self.check4,3,0)

        self.vlayout.setAlignment(QtCore.Qt.AlignTop)
        self.scrollArea.setWidget(self.newwidget)

class MyApp(QtGui.QMainWindow):

    def __init__(self, *args):
        super(MyApp, self).__init__(*args)
        self.scrollArea = QtGui.QScrollArea()

        self.initUI()

    def initUI(self):

        self.settingBtn = QtGui.QToolButton()
        self.settingBtn.setIcon(QtGui.QIcon("./img/setting.png"))
        self.settingBtn.setIconSize(QtCore.QSize(50,50))
        self.settingBtn.setStyleSheet("QToolButton { border: 0; background: transparent; width: 30px; height: 30px; }")
        self.settingBtn.setCheckable(True)
        self.settingBtn.setToolTip( _("Settings") )
        self.settingBtn.toggled.connect(self.showDock)

        self.powerBtn = QtGui.QToolButton()
        self.powerBtn.setIcon(QtGui.QIcon("./img/power1.png"))
        self.powerBtn.setIconSize(QtCore.QSize(50,50))
        self.powerBtn.setCheckable(True)
        self.powerBtn.setToolTip( _("Exit") )
        self.powerBtn.setStyleSheet("QToolButton { border: 0; background: transparent; width: 40px; height: 35px; }")
        self.powerBtn.toggled.connect(self.close)


        self.cb = QtGui.QComboBox()
        self.cb.addItems(["English", "German", "Arabic"])
        if lang == "English":
            self.cb.setCurrentIndex(0)
        elif lang == "German":
            self.cb.setCurrentIndex(1)
        elif lang == "Arabic":
            self.cb.setCurrentIndex(2)

        self.cb.currentIndexChanged.connect(self.change)



        self.timelabel = QtGui.QLabel()

        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.displayTime)
        self.timer.start()

        self.percentLbl = QtGui.QLabel()
        self.batteryIndicator = QtGui.QToolButton()

        self.timer2 = QtCore.QTimer(self)
        self.timer2.setInterval(200)
        self.timer2.timeout.connect(self.batteryStatus)
        self.timer2.start()

        self.wifi = QtGui.QToolButton()
        # self.wifi.setToolTip(x)

        self.timer3 = QtCore.QTimer(self)
        self.timer3.setInterval(1000)
        self.timer3.timeout.connect(self.wifiStatus)
        self.timer3.start()

        spacer = QtGui.QWidget(self)
        spacer.setSizePolicy(1|2|4,1|4)

        self.toolbar = QtGui.QToolBar(self)
        self.addToolBar(QtCore.Qt.TopToolBarArea, self.toolbar)
        self.toolbar.addWidget(self.settingBtn)
        self.toolbar.addSeparator()
        self.toolbar.addWidget(spacer)
        self.toolbar.addWidget(self.cb)

        self.toolbar.addSeparator()
        self.toolbar.addWidget(self.wifi)

        self.toolbar.addSeparator()
        self.toolbar.addWidget(self.batteryIndicator)
        self.toolbar.addWidget(self.percentLbl)
        self.toolbar.addSeparator()
        self.toolbar.addWidget(self.timelabel)
        self.toolbar.addSeparator()
        self.toolbar.addWidget(self.powerBtn)
        self.toolbar.setMovable(False)

        self.adminBtn = QtGui.QToolButton()
        self.adminBtn.setText( _("Admin") )
        self.adminBtn.setIcon(QtGui.QIcon("./img/admin.png"))
        self.adminBtn.setIconSize(QtCore.QSize(300,300))

        self.adminBtn.setStyleSheet("QToolButton { border: 0; background: transparent; width: 300px; height: 200px; }")


        self.scrollArea.setBackgroundRole(QtGui.QPalette.Light)
        #self.scrollArea.setWidget(self.textEdit)
        self.scrollArea.setWidgetResizable(True)

        self.scanBtn = QtGui.QPushButton( _("Scan") )
        self.scanBtn.setStyleSheet(ROUNDED_STYLE_SHEET3)

        self.icon1 = QtGui.QIcon()
        self.icon1.addPixmap(QtGui.QPixmap("./img/scan.png"),QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.scanBtn.setIcon(self.icon1)
        self.scanBtn.setIconSize(QtCore.QSize(40,40))

        self.scanBtn.clicked.connect(self.scanItem)

        self.previewBtn = QtGui.QPushButton( _("Preview") )
        self.previewBtn.setStyleSheet(ROUNDED_STYLE_SHEET2)

        self.icon2 = QtGui.QIcon()
        self.icon2.addPixmap(QtGui.QPixmap("./img/preview.png"),QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.previewBtn.setIcon(self.icon2)
        self.previewBtn.setIconSize(QtCore.QSize(40,40))

        self.previewBtn.clicked.connect(self.closewindow)

        self.saveBtn = QtGui.QPushButton(_("Save") )
        self.saveBtn.setStyleSheet(ROUNDED_STYLE_SHEET1)

        self.icon3 = QtGui.QIcon()
        self.icon3.addPixmap(QtGui.QPixmap("./img/save.png"),QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.saveBtn.setIcon(self.icon3)
        self.saveBtn.setIconSize(QtCore.QSize(40,40))

        self.saveBtn.clicked.connect(self.closewindow)

        self.backBtn = QtGui.QPushButton( _("Back"))
        self.backBtn.setStyleSheet(ROUNDED_STYLE_SHEET4)

        self.icon4 = QtGui.QIcon()
        self.icon4.addPixmap(QtGui.QPixmap("./img/back.png"),QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.backBtn.setIcon(self.icon4)
        self.backBtn.setIconSize(QtCore.QSize(40,40))

        self.hbox = QtGui.QHBoxLayout()

        self.twoDBtn = QtGui.QPushButton( _("2D"))
        self.twoDBtn.setStyleSheet("QPushButton {background-color: #c609b7;border: none;color: white;width: 57px;padding: 15px 32px;text-align: center;text-decoration: none;font-size: 16px; margin: 4px 2px;}")
        self.twoDBtn.clicked.connect(self.show2D)


        self.threeDBtn = QtGui.QPushButton( _("3D"))
        self.threeDBtn.setStyleSheet("QPushButton {background-color: #b20808;border: none;color: white;width: 57px;padding: 15px 32px;text-align: center;text-decoration: none;font-size: 16px; margin: 4px 2px;}")
        self.threeDBtn.clicked.connect(self.show3D)

        self.hbox.addWidget(self.twoDBtn)
        self.hbox.addWidget(self.threeDBtn)

        self.lbl1 = QtGui.QLabel(_("Admin"))
        self.lbl1.setStyleSheet("QLabel { border: 0; background: transparent; font-size: 20px; }")

        self.vlbl = QtGui.QLabel(_("version 1.0"))

        self.vbox = QtGui.QVBoxLayout()

        self.vbox.addWidget(self.lbl1)
        self.vbox.addWidget(self.adminBtn)
        self.vbox.addWidget(self.scanBtn)
        self.vbox.addWidget(self.previewBtn)
        self.vbox.addWidget(self.saveBtn)
        self.vbox.addWidget(self.backBtn)
        self.vbox.addLayout(self.hbox)
        self.vbox.addWidget(self.vlbl,QtCore.Qt.AlignBottom)

        self.visualization = Visualization()

        self.ui = self.visualization.edit_traits(parent=self,kind='subpanel').control

        self.scrollArea.setWidget(self.ui)

        self.mainLayout = QtGui.QGridLayout()

        self.mainLayout.addLayout(self.vbox,0,0)
        self.mainLayout.addWidget(self.scrollArea,0,1)

        self.setCentralWidget(QtGui.QWidget(self))
        self.centralWidget().setLayout(self.mainLayout)

        self.showFullScreen()

    def showDock(self):
        self.dialog = Settings(self)
        self.dialog.show()


    def change(self):
        if self.cb.currentText() == "Arabic":
            global lang
            lang = "Arabic"


            ar = gettext.translation('base', localedir='locales', languages=['ar'])
            ar.install()
            _ = ar.gettext # Greeks

            self.close()

            m = MyApp()
            m.show()

            #app = QtGui.QApplication.instance()
            sys.exit(app.exec_())

        elif self.cb.currentText() == "German":
            #global lang
            lang = "German"

            de = gettext.translation('base', localedir='locales', languages=['de'])
            de.install()
            _ = de.gettext # German

            self.close()

            m = MyApp()
            m.show()


            #app = QtGui.QApplication.instance()
            sys.exit(app.exec_())

        elif self.cb.currentText() == "English":
            #global lang
            lang = "English"

            en = gettext.translation('base', localedir='locales', languages=['en'])
            en.install()
            _ = en.gettext # German

            self.close()

            m = MyApp()
            m.show()
            sys.exit(app.exec_())
            #change(lang)


    def displayTime(self):

        cur_time = datetime.strftime(datetime.now(), "%I:%M:%S %p ")
        self.timelabel.setText(cur_time)

    def closewindow(self):
        print "hello"

    def batteryStatus(self):
        battery = psutil.sensors_battery()
        plugged = battery.power_plugged
        percent = int(battery.percent)
        remaining = str((100 - percent)) + _("%  remaining to charge")
        #print percent
        if percent == 100:
            self.percentLbl.setText("100 %")
        else:
            self.percentLbl.setText(str(percent) + "%")

        if plugged:
            self.batteryIndicator.setIcon(QtGui.QIcon("./img/charge.jpg"))
            self.batteryIndicator.setToolTip(remaining)
        else:
            remaining = str(percent) + _("%  remaining")
            self.batteryIndicator.setToolTip(remaining)
            if percent >=90 :
                self.batteryIndicator.setIcon(QtGui.QIcon("./img/full.jpg"))

            elif percent >=75 and percent <=89:
                self.batteryIndicator.setIcon(QtGui.QIcon("./img/80.jpg"))

            elif percent >=50 and percent <=74:
                self.batteryIndicator.setIcon(QtGui.QIcon("./img/50.jpg"))

            elif percent >=20 and percent <=49:
                self.batteryIndicator.setIcon(QtGui.QIcon("./img/30.jpg"))

            elif percent >=5 and percent <=19:
                self.batteryIndicator.setIcon(QtGui.QIcon("./img/10.jpg"))

            else:
                self.batteryIndicator.setIcon(QtGui.QIcon("./img/empty.jpg"))



    def wifiStatus(self):


        internet_on()

        # print c

        if connected:
            range = str(int(round(float(quality[0]) / float(quality[1]) * 100))).rjust(3)
            # range = 70
            #print range
            if range >= 80:
                self.wifi.setIcon(QtGui.QIcon("./img/wifi.png"))

            elif range >=40 and  range < 80:
                self.wifi.setIcon(QtGui.QIcon("./img/mid2.png"))
            else:
                self.wifi.setIcon(QtGui.QIcon("./img/low.png"))
        else:
            self.wifi.setIcon(QtGui.QIcon())


    def show2D(self):
        self.newwidget = QtGui.QWidget()
        self.glayout = QtGui.QGridLayout(self.newwidget)

        self.sl = QtGui.QSlider(QtCore.Qt.Vertical)
        self.sl.setMinimum(2)
        self.sl.setMaximum(100)
        self.sl.setValue(100)
        self.sl.setTickPosition(QtGui.QSlider.TicksBothSides)
        self.sl.setTickInterval(5)
        self.sl.valueChanged.connect(self.valuechange)

        self.canvas = MplCanvas()

        self.glayout.addWidget(self.canvas,0,0)
        self.glayout.addWidget(self.sl,0,1)

        self.scrollArea.setWidget(self.newwidget)

    def show3D(self):
        self.visualization = Visualization()
        self.ui = self.visualization.edit_traits(parent=self,kind='subpanel').control
        self.scrollArea.setWidget(self.ui)

    def valuechange(self):
        global q
        q = self.sl.value()

        self.canvas = MplCanvas()
        self.glayout.addWidget(self.canvas,0,0)

        self.scrollArea.setWidget(self.newwidget)

    def scanItem(self):


        self.d = QtGui.QDialog()
        self.form = QtGui.QFormLayout(self.d)

        self.collbl = QtGui.QLabel("Columns:")
        self.rowlbl = QtGui.QLabel("Rows:")
        self.colspin = QtGui.QSpinBox()
        self.colspin.setMinimum(1)
        self.colspin.setValue(10)
        self.rowspin = QtGui.QSpinBox()
        self.rowspin.setMinimum(1)
        self.rowspin.setValue(7)



        self.insertbtn = QtGui.QPushButton("Insert")
        self.insertbtn.clicked.connect(self.insert)

        self.canclebtn = QtGui.QPushButton("Cancel")
        self.canclebtn.clicked.connect(self.d.close)

        self.form.addRow(self.collbl, self.colspin)
        self.form.addRow(self.rowlbl, self.rowspin)
        self.form.addRow( self.canclebtn,self.insertbtn)

        self.d.setWindowTitle("Enter Grids")
        self.d.setGeometry(350,200,300,100)
        self.d.setWindowModality(QtCore.Qt.ApplicationModal)
        self.d.exec_()

    def insert(self):
        global X,Y
        X = self.colspin.value()
        print X

        Y = self.rowspin.value()
        print Y
        self.d.close()
        print "Inserted col = {}, row = {}".format(X,Y)

        self.a = QS(self)
        self.b = QV(self)
        self.b.setScene(self.a)
        self.scrollArea.setWidget(self.b)



def internet_on():
    global connected
    connected = False
    for timeout in [1,5,10,15]:
        try:
            response=urllib2.urlopen('http://google.com',timeout=timeout)
            connected = True
            return True
        except urllib2.URLError as err: pass
        connected = False
    return False



def get_name(cell):

    return matching_line(cell,"ESSID:")[1:-1]

def get_quality(cell):

    global quality
    quality = matching_line(cell,"Quality=").split()[0].split('/')
    #print str(int(round(float(quality[0]) / float(quality[1]) * 100))).rjust(3) + " %"
    return str(int(round(float(quality[0]) / float(quality[1]) * 100))).rjust(3) + " %"



rules={"Name":get_name,"Quality":get_quality}

def matching_line(lines, keyword):
    """Returns the first matching line in a list of lines. See match()"""
    for line in lines:
        matching=match(line,keyword)
        if matching!=None:
            return matching
    return None

def match(line,keyword):
    """If the first part of line (modulo blanks) matches keyword,
    returns the end of that line. Otherwise returns None"""
    line=line.lstrip()
    length=len(keyword)
    if line[:length] == keyword:
        return line[length:]
    else:
        return None

def parse_cell(cell):
    """Applies the rules to the bunch of text describing a cell and returns the
    corresponding dictionary"""
    parsed_cell={}
    for key in rules:
        rule=rules[key]
        parsed_cell.update({key:rule(cell)})
    return parsed_cell



def main():

    cells=[[]]
    parsed_cells=[]

    proc = subprocess.Popen(["iwlist", interface, "scan"],stdout=subprocess.PIPE, universal_newlines=True)
    out, err = proc.communicate()


    for line in out.split("\n"):
        cell_line = match(line,"Cell ")
        if cell_line != None:
            cells.append([])
            line = cell_line[-27:]
        cells[-1].append(line.rstrip())

    cells=cells[1:]

    for cell in cells:
        parsed_cells.append(parse_cell(cell))

    app = QtGui.QApplication.instance()

    global ex
    ex = MyApp()
    sys.exit(app.exec_())


# if __name__ == '__main__':
#     main()

def change(lang):
    # pass
    # lang = raw_input("Enter Your lang:\t ")

#_ = gettext.gettext

    if lang == "Arabic":
        ar = gettext.translation('base', localedir='locales', languages=['ar'])
        ar.install()
        _ = ar.gettext # Greeks
        main()

    elif lang == "German":
        de = gettext.translation('base', localedir='locales', languages=['de'])
        de.install()
        _ = de.gettext # German

        main()

    else:
        en = gettext.translation('base', localedir='locales', languages=['en'])
        en.install()
        _ = en.gettext # English
        main()

change(lang)
